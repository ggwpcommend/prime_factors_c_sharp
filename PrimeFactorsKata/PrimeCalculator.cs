﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace PrimeFactorsKata
{
    public class PrimeCalculator
    {
        public static IEnumerable<int> FactorsOf(int number)
        {
            var result = new List<int>();
            var reminder = number;
            var divisor = 2;
            while (reminder > 1)
            {
                while (reminder % divisor == 0)
                {
                    result.Add(divisor);
                    reminder /= divisor;
                }

                divisor++;
            }
  
            return result ;
        }

        public static bool IsPrime(int number)
        {
            return FactorsOf(number).Count() == 1;
        }
    }
}
