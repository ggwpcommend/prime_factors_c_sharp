using System;
using System.Linq;
using System.Collections.Generic;
using PrimeFactorsKata;
using Xunit;

namespace Tests
{
    public class PrimeFactorstTest
    {
        [Fact]
        public void Factor()
        {
            Assert.Equal( Enumerable.Empty<int>(), PrimeCalculator.FactorsOf(1));
        }

        [Fact]
        public void FactorOfTwoShouldBeTwo()
        {
            Assert.Equal(new List<int> { 2 }, PrimeCalculator.FactorsOf(2));
        }

        [Fact]
        public void FactorOfThreeShouldBe3()
        {
            Assert.Equal(new List<int> { 3 } , PrimeCalculator.FactorsOf(3));
        }

        [Fact]
        public void FactorOfThreeShouldBe4()
        {
            Assert.Equal(new List<int> { 2, 2 }, PrimeCalculator.FactorsOf(4));
        }

        [Fact]
        public void FactorOfComplexSeriesBeComplexSeries()
        {
            Assert.Equal(new List<int> { 2, 2 , 3, 3, 5, 7, 11, 11, 13 }, PrimeCalculator.FactorsOf(2*2*3*3*5*7*11*11*13));
        }
    }
}
