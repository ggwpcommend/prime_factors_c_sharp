﻿using System;
using System.Collections.Generic;
using System.Text;
using PrimeFactorsKata;
using Xunit;

namespace Tests
{
    public class Prime
    {
        [Fact]
        public void TwoIsPrime()
        {
            Assert.Equal(true, PrimeCalculator.IsPrime(2));
        }

        [Fact]
        public void ThreeIsPrime()
        {
            Assert.Equal(true, PrimeCalculator.IsPrime(3));
        }

        [Fact]
        public void FourIsnotPrime()
        {
            Assert.NotEqual(true, PrimeCalculator.IsPrime(4));
        }

        [Fact]
        public void OneHundredNinetyNineIsPrime()
        {
            Assert.Equal(true, PrimeCalculator.IsPrime(199));
        }


    }
}
